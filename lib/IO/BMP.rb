# The module BMP can write and read BMP images
module BMP
	
	MAGIC_NUMBER = 'BM'
	PIXEL_ARRAY_OFFSET = 54
	BITS_PER_PIXEL     = 24
	DIB_HEADER_SIZE    = 40
	PIXELS_PER_METER   = 2835

	# Write the image into a bmp file
	class Writer
		
		# Initialize a new writer with height, width and an array of hexadecimal value with 'bgr' format
		def initialize(height,width,pixels)
			@width, @height = width, height
			@pixels = pixels
		end

		# Define the size of the pixel array in the bmp file
		def pixel_array_size
			((BMP::BITS_PER_PIXEL*@width)/32.0).ceil*4*@height
		end

		# Save the bmp into a specific filename
		def save(filename)
			File.open(filename, 'wb') do |file|
				file << header
				file << dib
				write_pixels(file)
			end
		end

		# Get the header of the BMP
		def header
			[BMP::MAGIC_NUMBER, file_size, 0, 0, BMP::PIXEL_ARRAY_OFFSET].pack('A2Vv2V')
		end

		# Get the dib header of the BMP
		def dib
			[BMP::DIB_HEADER_SIZE, @width, @height, 1, BMP::BITS_PER_PIXEL, 0, pixel_array_size, 
		     BMP::PIXELS_PER_METER, BMP::PIXELS_PER_METER, 0, 0].pack('V3v2V6')
		end

		# Write pixels into the file
		def write_pixels(file)
			@pixels.reverse_each do |row|
				row.each do |color|
					file << pixel_bindstring(color)
				end
				file << row_padding
			end
		end

		# Format a pixel 
		def pixel_bindstring(rgb_string)
			raise ArgumentError unless rgb_string =~ /\A\h{6}\z/
			[rgb_string].pack("H6")
		end
		
		# Do the row_padding
		def row_padding
			"\x0" * (@width % 4)
		end

		# Get the final size of the bitmap file
		def file_size
			BMP::PIXEL_ARRAY_OFFSET + pixel_array_size
		end

		private :write_pixels, :pixel_bindstring, :row_padding

		# Width of the BMP
		attr_reader :width
		# Height of the BMP
		attr_reader :height
	end

	# Read a BMP file
	class Reader
		
		# Create a new reader with the filename
		def initialize(filename)
			File.open(filename, 'rb') do |file|
				read_header(file)
				read_dib(file)
				read_pixels(file)
			end
		end

		# Get the hexadecimal value ("bgr") of the specified bitmap pixel
		def [](x,y)
			@pixels[y][x]
		end

		# Some verifications with header
		def read_header(file)
			magic_number = file.read(2).unpack('A2').first
			size = file.read(4).unpack('V').first
			reserved = file.read(4).unpack('V').first
			pixel_array_offset = file.read(4).unpack('V').first


			fail 'Invalid BMP file : Your magic number is not valid !' unless magic_number == BMP::MAGIC_NUMBER
			fail 'Invalid BMP file : The size is not valid !' unless size == file.size
			fail 'Invalid BMP file !' unless reserved == 0
			fail 'Unsupported BMP file : Your pixel array offset is not supported' unless pixel_array_offset == BMP::PIXEL_ARRAY_OFFSET
		end

		# Read the DIB header of the bitmap file
		def read_dib(file)
			dib_header = file.read(40)

			dib_header_size, width, height, planes, bits_per_pixel, compression_method, image_size, hres,
			vres, n_colors, i_colors = dib_header.unpack("V3v2V6")

			unless dib_header_size == DIB_HEADER_SIZE
				fail 'Unsupported bitmap : DIB header size is not supported'
			end

			unless planes == 1
				fail 'Unsupported bitmap : Just 1 plane bitmap are supported'
			end

			unless bits_per_pixel == BITS_PER_PIXEL
				fail 'Unsupported bitmap : #{bits_per_pixel} bits per pixel bitmaps are not supported'
			end

			@width, @height = width, height
		end

		# Read the pixels of the bitmap
		def read_pixels(file)
			@pixels = Array.new(@height) { Array.new(@width) }

			(@height-1).downto(0) do |y|
				0.upto(@width-1) do |x|
					@pixels[y][x] = file.read(3).unpack('H6').first
				end
				advance_to_next_row(file)
			end
		end

		# Ignore the 0 in the bmp file
		def advance_to_next_row(file)
			padding_bytes = @width % 4
			return if padding_bytes == 0
	
			file.pos += padding_bytes
		end

		private :read_header, :read_pixels, :read_dib, :advance_to_next_row

		# Width of the bmp file
		attr_reader :width
		# Height of the bmp file 
		attr_reader :height 
		# Pixels (in hexadecimal value) array 
		attr_reader :pixels
	end
end