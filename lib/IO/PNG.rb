require 'zlib'

# The module PNG can read and write png images
module PNG
	# Header of a png image
	HEADER = [137, 'PNG', 2573, 26, 10].pack('CA3vC2')

	# Represent a chunk in a png image
	class Chunk
		# Type of the chunk
		attr_accessor :type

		# Initialize chunk with type
		def initialize(type)
			@type = type
		end
	
		#  crc32 chunk verification
		def crc
			[Zlib.crc32(data,Zlib.crc32(@type))].pack('N')
		end

		# Return the chunk bytes formated like in png file
		def pack
			str = ''
			str << [length, @type].pack('L>A4')
			str << data
			str << crc
			return str
		end

		# Data in chunk
		def data
			''
		end

		# Size of the data in a chunk
		def length
			data.length
		end
	end

	# Chunk with different information about the image
	class IHDR < Chunk
		# Width of the image
		attr_accessor :width
		# Height of the image
		attr_accessor :height
		#Bit depth of the image, for now only 8bit png images are supported
		attr_accessor :bit_depth
		#Color type of the image, for now only 2 (rgb images) and 6 (argb images) are supported
		attr_accessor :color_type
		# Adam7 interlace or not (1 or 0)
		attr_accessor :interlace

		# Initialize a new chunk with the different information
		def initialize(height, width, bit_depth, color_type, interlace)
			@width, @height, @bit_depth, @color_type, @interlace = width, height, bit_depth, color_type, interlace
			@type = 'IHDR'
		end

		# Create IDHR chunk from bytes
		def IHDR.from_data(data)
			width, height, bit_depth, color_type, compression_method, filter_method ,interlace = data.unpack('L>2c5')
			return IDHR.new(width, height, bit_depth, color_type, interlace)
		end

		def data 
			[@width,@height,@bit_depth,@color_type,0,0,0,@interlace].pack('L>2c5')
		end
	end

	# Chunk that contains the pixels of the image
	class IDAT < Chunk
		
		# Initialize IDAT chunk with the uncompressed data
		def initialize(uncompressed_data)
			@uncompressed_data = uncompressed_data
			@type              = 'IDAT'
		end

		# Create IDAT chunk from the compressed data
		def IDAT.from_data(data)
			z = Zlib::Inflate.new
			uncompressed_data = z.inflate(data, Zlib::FINISH)
			return IDAT.new(uncompressed_data)
		end

		def data
			z = Zlib::Deflate.new
			return z.deflate(@uncompressed_data, Zlib::FINISH)
		end
	end

	# Write an array of colors into png file
	class Writer
		# Intialize new writer with width and height
		def initialize(height,width,pixels)
			@ihdr = IHDR.new(width,height,8,6,0)
			0.upto(height-1) do |i|
				pixels.insert(i*width+i,'00')
			end
			uncompressed_data = [pixels.join('')].pack('H*')
			@idat = IDAT.new(uncompressed_data)
			@iend = Chunk.new('IEND')
		end

		# Save the file into filename
		def save_as(filename)
			File.open(filename, 'wb') do |file|
				file << PNG::HEADER
				file << @ihdr.pack
				file << @idat.pack
				file << @iend.pack
			end
		end
	end
end