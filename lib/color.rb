# Color represent a pixel color with rgb pigments and a channel to represent transparancy
class Color

	# Initialize a new color, black by default.
	def initialize(r = 0, g = 0, b = 0, a = 255)
		@r, @g, @b, @a = r,g,b,a
	end

	# Initialize a new color from an hex value
	def Color.from_hex(str, args = 'rgb')
		c = Color.new
		c.from_hex(str, args)
		return c
	end

	# The hex value of a color
	def hex_value(args = 'rgb')
		str = ''
		args.split('').each do |arg|
			case arg
			when 'a'
				str += f_hex(@a)
			when 'r'
				str += f_hex(@r)
			when 'g'
				str += f_hex(@g)
			when 'b'
				str += f_hex(@b)
			else
				fail 'Argument invalid'
			end
		end
		return str
	end

	# Set the different components from an hexadecimal value
	def from_hex(str, args = 'rgb')
		c = []
		args = args.split('')
		0.step(str.length, 2) do |i|
			c.push(str[i..i+1])
		end
		c.map! {|e| e.to_i 16}
		args.each do |arg|
			case arg
			when 'a'
				@a = c.shift
			when 'r'
				@r = c.shift
			when 'g'
				@g = c.shift
			when 'b'
				@b = c.shift
			else
				fail 'Argument invalid'
			end
		end
	end

	# Red component (between 0 and 255)
	attr_accessor :r 
	# Green component (between 0 and 255)
	attr_accessor :g
	# Blue component (between 0 and 255)
	attr_accessor :b
	# Alpha component (represent transparency) (between 0 and 255)
	attr_accessor :a

	# Format an integer into a valid hexadecimal value (length multiple of two)
	def f_hex(num)
		str = num.to_s 16
		if str.length % 2 == 1 then str = '0' + str end
		return str
	end

	private :f_hex
end