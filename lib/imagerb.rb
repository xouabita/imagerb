require 'color'
require 'palette'
require 'IO/PNG'
require 'IO/BMP'

# The Image class is the main class of the project. It represent your image
# in a array of colors.
class Image
	
	# Initialize a new image, with height and width
	def initialize(height = 0, width = 0, color = Color.new)
		@width, @height = width, height
		@pixels = Array.new(width*height) { color }
	end
	
	public

	# Set the pixel at 0 ≤ x ≤ height and 0 ≤ y ≤ width
	def set_pixel(x,y,value)
		@pixels[x*@width+y] = value
	end

	# Get the pixel at 0 ≤ x ≤ height and 0 ≤ y ≤ width 
	def get_pixel(x,y)
		@pixels[x*@width+y]
	end

	# Another maneer to get a specific pixel
	def [](x,y)
		@pixels[x*@width+y]
	end

	# Another maneer to set a specific pixel
	def []=(x,y,value)
		@pixels[x*@width+y] = value
	end

	# Get the image array as an array of hexadecimal value
	def hex_array(arg = 'rgb')
		arr = Array.new(@height) { Array.new(@width) }
		0.upto(@height-1) do |i|
			0.upto(@width-1) do |j|
				arr[i][j] = @pixels[i*@width+j].hex_value(arg)
			end
		end
		return arr
	end

	# Get the image as an array of colors
	def color_array
		arr = Array.new(@height) { Array.new(@width) }
		0.upto(@height-1) do |i|
			0.upto(@width-1) do |j|
				arr[i][j] = @pixels[i*@width+j]
			end
		end
	end

	# Create an image with an hexadimal array
	def open_hex_array(array, args = 'rgb')
		@height = array.length
		@width  = array[0].length
		@pixels = Array.new(width*height)
		0.upto(@height-1) do |i|
			0.upto(@width-1) do |j|
				@pixels[i*@width+j] = Color.from_hex(array[i][j], args)
			end
		end
	end

	# Create an image with an image file
	def open(filename)
		case filename.split('.').last
		when 'bmp'
			open_bmp(filename)
		else
			fail 'Unsupported file format : #{filename.split(".").last} file is unsupported !'
		end

	end

	# Save your image into the specific format
	def save(filename)
		case filename.split('.').last
		when 'png'
			save_png(filename)
		when 'bmp'
			save_bmp(filename)
		else
			fail 'Unsupported file format : #{filename.split(".").last} file is unsupported !'
		end
	end

	# Get the height value
	attr_reader :height
	# Get the width value
	attr_reader :width

	private

	# Open a bmp image
	def open_bmp(filename)
		bmp = BMP::Reader.new(filename)
		puts bmp.pixels.to_s
		open_hex_array(bmp.pixels, 'bgr')
	end

	# Save the image as png
	def save_png(filename)
		hex_array = @pixels.map { |e| e.hex_value('rgba') }
		png = PNG::Writer.new(@height,@width,hex_array)
		png.save_as(filename)
	end

	# Save the image as bmp
	def save_bmp(filename)
		bmp = BMP::Writer.new(@height,@width,hex_array('bgr'))
		bmp.save(filename)
	end
end