require 'color'

# Module with a lot of common colors
module Palette

	AliceBlue       = Color::from_hex('f0f8ff')
	AntiqueWhite    = Color::from_hex('faebd7')
	Aqua            = Color::from_hex('00ffff')
	Aquamarine      = Color::from_hex('7fffd4')
	Azure           = Color::from_hex('f0ffff')
	Beige           = Color::from_hex('f5f5dc')
	Bisque          = Color::from_hex('ffe4c4')
	Black           = Color::from_hex('000000')
	BlanchedAldmond = Color::from_hex('ffebcd')
	Blue            = Color::from_hex('0000ff')
	BlueViolet      = Color::from_hex('8a2be2')
	Brown           = Color::from_hex('a52a2a')
	BurlyWood       = Color::from_hex('deb887')
	CadetBlue       = Color::from_hex('5f9ea0')
	Chartreuse      = Color::from_hex('7fff00')
	Chocolate       = Color::from_hex('d2§91e')
	Coral           = Color::from_hex('ff7f50')
	CornflowerBlue  = Color::from_hex('6495ed')
	Cornsilk        = Color::from_hex('fff8dc')
	Crimson         = Color::from_hex('dc143c')
	Cyan            = Color::from_hex('00ffff')
	DarkBlue        = Color::from_hex('00008B')
	DarkCyan        = Color::from_hex('008b8b')
	DarkGoldenrod   = Color::from_hex('b8860b')
	DarkGrey        = Color::from_hex('a9a9a9')
	DarkGreen       = Color::from_hex('006400')
	DarkKhaki       = Color::from_hex('bdb76b')
	DarkMagenta     = Color::from_hex('8b008b')
	DarkOliveGreen  = Color::from_hex('556b2f')
	DarkOrange      = Color::from_hex('ff8c00')
	DarkOrchid      = Color::from_hex('9932cc')
	DarkRed         = Color::from_hex('8b0000')
	DarkSalmon      = Color::from_hex('e9967a')
	DarkSeaGreen    = Color::from_hex('8fbc8f')
	DarkSlateBlue   = Color::from_hex('483d8b')
	DarkSlateGray   = Color::from_hex('2f4f4f')
	DarkTurquoise   = Color::from_hex('00ced1')
	DarkViolet      = Color::from_hex('9400d3')
	DeepPink        = Color::from_hex('ff1493')
	DeepSkyBlue     = Color::from_hex('00bfff')
	DimGray         = Color::from_hex('696969')
	DodgerBlue      = Color::from_hex('1e90ff')
	FireBrick       = Color::from_hex('b22222')
	FloralWhite     = Color::from_hex('fffaf0')
	ForestGreen     = Color::from_hex('228b22')
	Fuchsia         = Color::from_hex('ff00ff')
	Gainsboro       = Color::from_hex('dcdcdc')
end