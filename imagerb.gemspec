Gem::Specification.new do |s|
	s.name        = 'imagerb'
	s.version     = '0.0.1'
	s.date        = '2013-08-12'
	s.summary     = 'Image Library'
	s.description = 'Simple image library gem'   
	s.authors     = ['Alexandre Abita']
	s.email       = 'xouabita@gmail.com'
	s.files       = ['lib/imagerb.rb','lib/color.rb','lib/IO/PNG.rb','lib/IO/BMP.rb','lib/palette.rb']
	s.homepage    = 'https://bitbucket.org/xouabita/imagerb/'
	s.license     = 'Beerware' 
end