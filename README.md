RubyRB : Simple image library in Ruby
=====================================

What is RubyRB ?
----------------

RubyRB is a gem that allow you to play with images in ruby. 
At the moment, three formats are supported : JPG, PNG and BMP.
An Image is an array of pixels, represented by the Color class.

How to begin ?
--------------

You can create a new image like that :
	
	img = Image.new

Your image is by default a black image with 0x0 dimension. To create a bigger image, you can specify the width and height :

	img = Image.new(10,10)

Here you created a 10x10 black image. You can also create a new image with a background color :

	img = Image.new(10,10,Palette::Beige)

You can also get a specific pixel (0 ≤ x ≤ height, 0 ≤ y ≤ width) :

	c = img[x,y]

A pixel is a color, with three pigment and a transparancy :

-  r : Red
-  g : Green
-  b : Blue
-  a : Alpha

You can create a color from scratch with r,g,b value or with hexadecimal value, or use a color present in the palette. A pigment value is between 0 -> 255

	red = Color.new(255,0,0) 
	green = Color::from_hex("00ff00")

You can set your color to a specific destination in the image :

	img[5,5] = red
	img[7,8] = green

And at least, just save your image :

	img.save("beautiful_img.bmp")